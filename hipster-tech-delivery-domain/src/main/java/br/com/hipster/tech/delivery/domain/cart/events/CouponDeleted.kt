package br.com.hipster.tech.delivery.domain.cart.events

import br.com.hipster.tech.delivery.domain.cart.commons.CartApply
import br.com.hipster.tech.delivery.domain.cart.commons.CartEvent
import br.com.zup.eventsourcing.core.Event

class CouponDeleted : Event(), CartEvent {
    override fun accept(cart: CartApply) { cart.apply(this) }
}
