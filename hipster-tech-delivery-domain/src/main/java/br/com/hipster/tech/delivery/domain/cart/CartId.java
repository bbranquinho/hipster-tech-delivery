package br.com.hipster.tech.delivery.domain.cart;

import br.com.zup.eventsourcing.core.AggregateId;

public class CartId extends AggregateId {

    public CartId(String value) {
        super(value);
    }

}
