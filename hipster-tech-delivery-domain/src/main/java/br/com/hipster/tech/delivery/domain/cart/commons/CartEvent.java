package br.com.hipster.tech.delivery.domain.cart.commons;

import br.com.hipster.tech.delivery.domain.cart.commons.CartApply;

public interface CartEvent {

    void accept(CartApply cart);

}
