package br.com.hipster.tech.delivery.domain.cart;

public class Coupon {

    private final String code;

    public Coupon(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
