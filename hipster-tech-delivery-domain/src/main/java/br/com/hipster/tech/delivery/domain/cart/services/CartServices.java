package br.com.hipster.tech.delivery.domain.cart.services;

public interface CartServices {

    boolean payCart(String cartId, Double totalPrice);

}
