package br.com.hipster.tech.delivery.domain.customer.events

import br.com.hipster.tech.delivery.domain.customer.Address
import br.com.hipster.tech.delivery.domain.customer.Detail
import br.com.zup.eventsourcing.core.Event

data class CustomerCreated(val detail: Detail) : Event()

data class AddressUpdated(val address: Address) : Event()
