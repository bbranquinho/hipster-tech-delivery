package br.com.hipster.tech.delivery.domain.customer;

import java.math.BigDecimal;

public class Address {

    private final String streetAddress;

    private final String state;

    private final String city;

    private final String zipCode;

    private final BigDecimal latitude;

    private final BigDecimal longitude;

    public Address(String streetAddress, String state, String city, String zipCode, BigDecimal latitude, BigDecimal longitude) {
        this.streetAddress = streetAddress;
        this.state = state;
        this.city = city;
        this.zipCode = zipCode;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }
}
