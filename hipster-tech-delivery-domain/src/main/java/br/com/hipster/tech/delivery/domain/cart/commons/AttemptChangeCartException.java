package br.com.hipster.tech.delivery.domain.cart.commons;

public class AttemptChangeCartException extends RuntimeException {

    public AttemptChangeCartException() {
    }

    public AttemptChangeCartException(String message) {
        super(message);
    }

    public AttemptChangeCartException(String message, Throwable cause) {
        super(message, cause);
    }

    public AttemptChangeCartException(Throwable cause) {
        super(cause);
    }

    public AttemptChangeCartException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
