package br.com.hipster.tech.delivery.domain.cart;

import br.com.hipster.tech.delivery.domain.cart.commons.CartApply;
import br.com.hipster.tech.delivery.domain.cart.commons.CartEvent;
import br.com.hipster.tech.delivery.domain.cart.events.*;
import br.com.hipster.tech.delivery.domain.cart.commons.AttemptChangeCartException;
import br.com.hipster.tech.delivery.domain.cart.services.CartServices;
import br.com.zup.eventsourcing.core.AggregateRoot;
import br.com.zup.eventsourcing.core.Event;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CartAggregate extends AggregateRoot {

    private final CartApply visitor = new EventApplier();

    private CartStatus status;

    private String customerId;

    private Coupon coupon;

    private Map<Long, Product> products = new HashMap();

    public CartAggregate(String customerId) {
        CartId cartId = new CartId(UUID.randomUUID().toString());
        CartCreated event = new CartCreated(cartId, customerId, CartStatus.NEW);

        applyChange(event);
    }

    public void applyEvent(Event event) {
        ((CartEvent) event).accept(this.visitor);
    }

    public void addProduct(Product product) {
        attemptChangeCart();
        applyChange(new ProductAdded(product));
    }

    public void deleteProduct(Long productId) {
        attemptChangeCart();
        applyChange(new ProductDeleted(productId));
    }

    public void addCoupon(Coupon coupon) {
        attemptChangeCart();
        applyChange(new CouponAdded(coupon));
    }

    public void deleteCoupon() {
        attemptChangeCart();
        applyChange(new CouponDeleted());
    }

    public void executeCheckout(CartServices cartServices) {
        attemptChangeCart();

        Double totalPrice = products
                .values()
                .stream()
                .mapToDouble(p -> p.getUnitPrice() * p.getAmount())
                .sum();

        if (cartServices.payCart(id.getValue(), totalPrice)) {
            applyChange(new PaymentAuthorized());
        } else {
            applyChange(new PaymentFailed());
        }
    }

    public String getCartIt() {
        return id.getValue();
    }

    public CartStatus getStatus() {
        return status;
    }

    public String getCustomerId() {
        return customerId;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public Map<Long, Product> getProducts() {
        return products;
    }

    private void attemptChangeCart() {
        if (status != CartStatus.NEW) {
            throw new AttemptChangeCartException("It is not allowed change a cart with status " + status);
        }
    }

    private class EventApplier implements CartApply {

        public void apply(CartCreated event) {
            id = event.getCartId();
            customerId = event.getCustomerId();
            status = event.getStatus();
        }

        public void apply(ProductAdded event) {
            products.put(event.getProductId(), event.getProduct());
        }

        public void apply(ProductDeleted event) {
            products.remove(event.getProductId());
        }

        public void apply(CouponAdded event) {
            coupon = event.getCoupon();
        }

        public void apply(CouponDeleted event) {
            coupon = null;
        }

        public void apply(PaymentAuthorized event) {
            status = CartStatus.AUTHORIZED;
        }

        public void apply(PaymentFailed event) {
            status = CartStatus.FAILED;
        }

    }

}
