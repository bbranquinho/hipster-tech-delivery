package br.com.hipster.tech.delivery.domain.cart.commands;

import br.com.hipster.tech.delivery.domain.cart.CartId;

public class AddProductCommand {

    private final CartId cartId;

    private final Long id;

    private final String name;

    private final String description;

    private final Double unitPrice;

    private final Double amount;

    public AddProductCommand(String cartId, Long id, String name, String description, Double unitPrice, Double amount) {
        this.cartId = new CartId(cartId);
        this.id = id;
        this.name = name;
        this.description = description;
        this.unitPrice = unitPrice;
        this.amount = amount;
    }

    public AddProductCommand(CartId cartId, Long id, String name, String description, Double unitPrice, Double amount) {
        this.cartId = cartId;
        this.id = id;
        this.name = name;
        this.description = description;
        this.unitPrice = unitPrice;
        this.amount = amount;
    }

    public CartId getCartId() {
        return cartId;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public Double getAmount() {
        return amount;
    }
}
