package br.com.hipster.tech.delivery.domain.customer;

public class Detail {

    private final String firstName;

    private final String lastName;

    private final String email;

    public Detail(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }
}
