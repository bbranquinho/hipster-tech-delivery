package br.com.hipster.tech.delivery.domain.cart.commands;

public class CreateCartCommand {

    private String customerId;

    public CreateCartCommand(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }
}
