package br.com.hipster.tech.delivery.domain.cart;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum CartStatus {
    AUTHORIZED("Authorized"),
    CANCELLED("Cancelled"),
    CHARGED_BACK("Charged Back"),
    FAILED("Failed"),
    EXPIRED("Expired"),
    REFUND("Refund"),
    NEW("New");

    private final String status;

    private static final Map<String, CartStatus> MAP_STATUS;

    static {
        MAP_STATUS = Arrays.stream(values()).collect(Collectors.toMap(k -> k.status.toUpperCase(), v -> v));
    }

    CartStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }

    public CartStatus parse(String status) {
        if (status == null) {
            return null;
        }

        return MAP_STATUS.get(status.toUpperCase());
    }
}
