package br.com.hipster.tech.delivery.domain.cart.commons;

import br.com.hipster.tech.delivery.domain.cart.events.*;

public interface CartApply {

    void apply(CartCreated event);

    void apply(ProductAdded event);

    void apply(ProductDeleted event);

    void apply(CouponAdded event);

    void apply(CouponDeleted event);

    void apply(PaymentAuthorized event);

    void apply(PaymentFailed event);

}
