package br.com.hipster.tech.delivery.domain.customer;

import br.com.hipster.tech.delivery.domain.customer.events.AddressUpdated;
import br.com.hipster.tech.delivery.domain.customer.events.CustomerCreated;
import br.com.zup.eventsourcing.core.AggregateRoot;
import br.com.zup.eventsourcing.core.Event;

public class CustomerAggregate extends AggregateRoot {

    private Address address;

    private Detail detail;

    public void applyEvent(Event event) {
        if (event instanceof CustomerCreated) handle((CustomerCreated)event);
        if (event instanceof AddressUpdated) handle((AddressUpdated) event);
    }

    private void handle(CustomerCreated event) {
        detail = event.getDetail();
    }

    private void handle(AddressUpdated event) {
        address = event.getAddress();
    }

    public Address getAddress() {
        return address;
    }

    public Detail getDetail() {
        return detail;
    }
}
