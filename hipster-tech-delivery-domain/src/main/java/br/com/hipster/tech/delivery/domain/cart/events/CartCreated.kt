package br.com.hipster.tech.delivery.domain.cart.events

import br.com.hipster.tech.delivery.domain.cart.CartId
import br.com.hipster.tech.delivery.domain.cart.CartStatus
import br.com.hipster.tech.delivery.domain.cart.commons.CartApply
import br.com.hipster.tech.delivery.domain.cart.commons.CartEvent
import br.com.zup.eventsourcing.core.Event

data class CartCreated(val cartId: CartId, val customerId: String, val status: CartStatus) : Event(), CartEvent {
    override fun accept(cart: CartApply) { cart.apply(this) }
}
