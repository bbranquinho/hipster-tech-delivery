package br.com.hipster.tech.delivery.domain.cart.events

import br.com.hipster.tech.delivery.domain.cart.Product
import br.com.hipster.tech.delivery.domain.cart.commons.CartApply
import br.com.hipster.tech.delivery.domain.cart.commons.CartEvent
import br.com.zup.eventsourcing.core.Event

data class ProductAdded(val product: Product) : Event(), CartEvent {
    override fun accept(cart: CartApply) {
        cart.apply(this)
    }

    fun getProductId(): Long =
            product.id
}
