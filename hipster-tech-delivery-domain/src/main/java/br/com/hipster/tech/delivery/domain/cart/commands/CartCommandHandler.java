package br.com.hipster.tech.delivery.domain.cart.commands;

import br.com.hipster.tech.delivery.domain.cart.CartAggregate;
import br.com.hipster.tech.delivery.domain.cart.Product;
import br.com.zup.eventsourcing.core.Repository;
import br.com.zup.eventsourcing.eventstore.EventStoreRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CartCommandHandler {

    private static final Logger logger = LoggerFactory.getLogger(CartCommandHandler.class);

    private final EventStoreRepository<CartAggregate> cartEventRepository;

    public CartCommandHandler(EventStoreRepository<CartAggregate> cartEventRepository) {
        this.cartEventRepository = cartEventRepository;
    }

    public CartAggregate handle(CreateCartCommand createCartCommand) {
        CartAggregate cart = new CartAggregate(createCartCommand.getCustomerId());
        cartEventRepository.save(cart, Repository.OptimisticLock.ENABLED);

        logger.debug("Cart [{}] created for customer [{}].", cart.getCartIt(), cart.getCustomerId());

        return cart;
    }

    public void handle(AddProductCommand addProductCommand) {
        CartAggregate cart = cartEventRepository.find(addProductCommand.getCartId());

        Product product = new Product(addProductCommand.getId(), addProductCommand.getName(),
                addProductCommand.getDescription(), addProductCommand.getUnitPrice(), addProductCommand.getAmount());

        cart.addProduct(product);

        cartEventRepository.save(cart, Repository.OptimisticLock.ENABLED);
    }

}
