package br.com.hipster.tech.delivery.domain.cart

import br.com.hipster.tech.delivery.domain.assertException
import br.com.hipster.tech.delivery.domain.cart.commons.AttemptChangeCartException
import br.com.hipster.tech.delivery.domain.cart.services.CartServices
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

class CartAggregateTest {

    private val cartServices = mock<CartServices> {
        on { payCart(any(), eq(20.00)) } doReturn true
        on { payCart(any(), eq(45.00)) } doReturn false
    }

    private val rice = Product(1L, "Rice", "Delicious rice", 10.00, 2.0)

    private val bean = Product(2L, "Bean", "Fresh bean", 15.00, 3.0)

    private val coupon = Coupon("BR102A33C")

    @Test
    fun `Should create a new cart with success`() {
        val cart = CartAggregate("customerId")

        assertNotNull(cart.id)

        assertNull(cart.coupon)

        assertEquals(expected = cart.customerId, actual = "customerId")
        assertEquals(expected = CartStatus.NEW, actual = cart.status)

        assertTrue(cart.products.isEmpty())
    }

    @Test
    fun `Add a new product with success`() {
        val cart = CartAggregate("customerId")

        cart.addProduct(rice)

        assertNotNull(cart.id)
        assertNull(cart.coupon)

        assertEquals(expected = cart.customerId, actual = "customerId")
        assertEquals(expected = CartStatus.NEW, actual = cart.status)
        assertEquals(expected = mapOf(1L to rice), actual = cart.products)
    }

    @Test
    fun `Add two product and posteriorly remove one of them with success`() {
        val cart = CartAggregate("customerId")

        cart.addProduct(rice)
        cart.addProduct(bean)

        cart.deleteProduct(rice.id)

        assertNotNull(cart.id)
        assertNull(cart.coupon)

        assertEquals(expected = cart.customerId, actual = "customerId")
        assertEquals(expected = CartStatus.NEW, actual = cart.status)
        assertEquals(expected = mapOf(2L to bean), actual = cart.products)
    }

    @Test
    fun `Add a product and execute checkout with success`() {
        val cart = CartAggregate("customerId")

        cart.addProduct(rice)
        cart.executeCheckout(cartServices)

        assertTrue(cart.status == CartStatus.AUTHORIZED)
    }

    @Test
    fun `Add a product and try to pay, the payment is refused`() {
        val cart = CartAggregate("customerId")

        cart.addProduct(bean)
        cart.executeCheckout(cartServices)

        assertTrue(cart.status == CartStatus.FAILED)
    }

    @Test
    fun `Try to add a new product to a cart that is already finished, it needs throws an AttemptChangeCartException`() {
        val cart = CartAggregate("customerId")

        cart.addProduct(rice)
        cart.executeCheckout(cartServices)

        assertException(
                expectedException = AttemptChangeCartException::class.java,
                expectedMessage = "It is not allowed change a cart with status Authorized",
                code = {
                    cart.addProduct(bean)
                }
        )
    }

    @Test
    fun `Add a coupon with success`() {
        val cart = CartAggregate("customerId")

        cart.addCoupon(coupon)

        assertNotNull(cart.id)
        assertNotNull(cart.coupon)

        assertEquals(expected = cart.customerId, actual = "customerId")
        assertEquals(expected = CartStatus.NEW, actual = cart.status)

        assertTrue(cart.products.isEmpty())
    }

    @Test
    fun `Delete a coupon with success`() {
        val cart = CartAggregate("customerId")

        cart.addCoupon(coupon)
        cart.deleteCoupon()

        assertNotNull(cart.id)

        assertNull(cart.coupon)

        assertEquals(expected = cart.customerId, actual = "customerId")
        assertEquals(expected = CartStatus.NEW, actual = cart.status)
    }

}