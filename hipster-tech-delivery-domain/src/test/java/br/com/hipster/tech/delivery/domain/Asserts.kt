package br.com.hipster.tech.delivery.domain

import kotlin.test.assertTrue

fun <E : Throwable, T> T.assertException(expectedException: Class<E>, expectedMessage: String, code: T.() -> Any) =
        try {
            code()
        } catch (e: Throwable) {
            assertTrue(
                    "The expected exception is [${expectedException.canonicalName}], but throws [${e.javaClass.canonicalName}].",
                    { expectedException == e.javaClass }
            )
            assertTrue(
                    "The expected message is [$expectedMessage], but throws the message [${e.message}].",
                    { expectedMessage == e.message }
            )
        }
