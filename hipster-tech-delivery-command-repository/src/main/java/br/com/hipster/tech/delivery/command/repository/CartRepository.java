package br.com.hipster.tech.delivery.command.repository;

import br.com.hipster.tech.delivery.domain.cart.CartAggregate;
import br.com.zup.eventsourcing.eventstore.EventStoreRepository;
import org.springframework.stereotype.Service;

@Service
public class CartRepository extends EventStoreRepository<CartAggregate> {
}
