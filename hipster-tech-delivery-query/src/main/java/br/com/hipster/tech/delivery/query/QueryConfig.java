package br.com.hipster.tech.delivery.query;

import br.com.hipster.tech.delivery.domain.cart.CartAggregate;
import br.com.hipster.tech.delivery.domain.cart.commands.CartCommandHandler;
import br.com.zup.eventsourcing.eventstore.EventStoreRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@ComponentScan(basePackages = "br.com.hipster.tech.delivery")
@Configuration
public class QueryConfig {
}
