package br.com.hipster.tech.delivery.command.resource

import br.com.hipster.tech.delivery.domain.cart.CartAggregate
import br.com.hipster.tech.delivery.domain.cart.commands.CartCommandHandler
import br.com.hipster.tech.delivery.domain.cart.commands.CreateCartCommand
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@EnableAutoConfiguration
@ComponentScan(basePackages = ["br.com.hipster.tech.delivery.command.resource"])
@Configuration
class CommandResourceConfigTest {

    private val cartAggregate = CartAggregate("1")

    private val cartCommandHandler = mock<CartCommandHandler> {
        on { handle(any<CreateCartCommand>()) } doReturn cartAggregate
    }

    @Bean
    fun cartCommandHandler(): CartCommandHandler =
            cartCommandHandler

}
