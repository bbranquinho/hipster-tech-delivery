package br.com.hipster.tech.delivery.command.resource.cart

import br.com.hipster.tech.delivery.command.resource.CommandResourceBaseTest
import org.hamcrest.CoreMatchers
import org.junit.Test
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation
import org.springframework.restdocs.payload.PayloadDocumentation
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class CartResourceTest : CommandResourceBaseTest() {

    companion object {
        const val CUSTOMER_ID_DESCRIPTION = "ID that identifies the customer that is building the cart."
        const val CART_ID_DESCRIPTION = "ID that identifies the cart that was created."
    }

    @Test
    fun `Create a cart for a customer`() {
        val cartRequestContent = """{"customerId":"1"}"""

        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/cart")
                .content(cartRequestContent)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isCreated)
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.notNullValue()))
                .andDo(
                        MockMvcRestDocumentation.document("{method-name}",
                                PayloadDocumentation.requestFields(
                                        PayloadDocumentation.fieldWithPath("customerId").description(CUSTOMER_ID_DESCRIPTION)
                                ),
                                PayloadDocumentation.responseFields(
                                        PayloadDocumentation.fieldWithPath("id").description(CART_ID_DESCRIPTION)
                                )
                        )
                )
    }

}