package br.com.hipster.tech.delivery.command.resource.cart;

public class CartRequest {

    private String customerId;

    public CartRequest() {
    }

    public CartRequest(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }
}
