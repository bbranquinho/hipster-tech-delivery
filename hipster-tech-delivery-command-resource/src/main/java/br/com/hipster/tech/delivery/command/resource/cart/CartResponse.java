package br.com.hipster.tech.delivery.command.resource.cart;

public class CartResponse {

    private String id;

    public CartResponse(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
