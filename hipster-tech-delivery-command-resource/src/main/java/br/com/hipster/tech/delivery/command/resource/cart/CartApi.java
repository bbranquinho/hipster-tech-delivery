package br.com.hipster.tech.delivery.command.resource.cart;

import br.com.hipster.tech.delivery.command.resource.commons.Paths;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RequestMapping(path = Paths.CART_PATH)
public interface CartApi {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    CartResponse create(@RequestBody CartRequest cartRequest);

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(path = "/{id}/product")
    void addProduct(@PathVariable("id") String id, ProductRequest productRequest);

}
