package br.com.hipster.tech.delivery.command.resource.commons;

public final class Paths {

    private Paths() { }

    public static final String ROOT_PATH = "/api/v1";

    public static final String CART_PATH = ROOT_PATH + "/cart";

}
