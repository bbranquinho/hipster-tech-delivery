package br.com.hipster.tech.delivery.command.resource.cart;

import br.com.hipster.tech.delivery.domain.cart.CartAggregate;
import br.com.hipster.tech.delivery.domain.cart.commands.AddProductCommand;
import br.com.hipster.tech.delivery.domain.cart.commands.CartCommandHandler;
import br.com.hipster.tech.delivery.domain.cart.commands.CreateCartCommand;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CartResource implements CartApi {

    private final CartCommandHandler cartCommandHandler;

    public CartResource(CartCommandHandler cartCommandHandler) {
        this.cartCommandHandler = cartCommandHandler;
    }

    @Override
    public CartResponse create(@RequestBody CartRequest cartRequest) {
        CreateCartCommand createCartCommand = new CreateCartCommand(cartRequest.getCustomerId());
        CartAggregate cartAggregate = cartCommandHandler.handle(createCartCommand);
        return new CartResponse(cartAggregate.getCartIt());
    }

    @Override
    public void addProduct(@PathVariable("id") String id, @RequestBody ProductRequest productRequest) {
        AddProductCommand addProductCommand = new AddProductCommand(id, productRequest.getId(), productRequest.getName(),
                productRequest.getDescription(), productRequest.getUnitPrice(), productRequest.getAmount());
        cartCommandHandler.handle(addProductCommand);
    }

}
