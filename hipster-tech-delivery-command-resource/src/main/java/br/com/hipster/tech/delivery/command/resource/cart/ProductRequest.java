package br.com.hipster.tech.delivery.command.resource.cart;

public class ProductRequest {

    private final Long id;

    private final String name;

    private final String description;

    private final Double unitPrice;

    private final Double amount;

    public ProductRequest(Long id, String name, String description, Double unitPrice, Double amount) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.unitPrice = unitPrice;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public Double getAmount() {
        return amount;
    }
}
